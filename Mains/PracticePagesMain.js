window.onload = init;

function init() {
    document.NODE_SIZE = 30;
    canvas = document.getElementById('myCanvas');
    ctx = canvas.getContext('2d');
    dll = new BST(400,400);
    dll.push(4);
    dll.push(3);
    dll.push(5);
    dll.push(6);
    dll.push(2);
    dll.push(7);
    dll.push(8);
    setInterval(update,50);
}

function update() {
    ctx.fillStyle = "white";
    ctx.fillRect(0,0,canvas.width,canvas.height);
    dll.refresh(ctx,4);
}
