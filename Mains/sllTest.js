function runLoop() {
    context.fillStyle = "white";
    context.fillRect(0,0,canvas.width,canvas.height);
    console.log("filled background "+counter);
    structure.refresh(context,4);
    if (counter % 70 === 0)
    {
        structure.pop();
    }
    counter += 1;
}



function initialize() {
    document.NODE_SIZE = 30;
    canvas = document.getElementById('canv');
    context = canvas.getContext('2d');
    structure = new SinglyLinkedList(400,400,new StackOrdering());
    structure.push(4);
    structure.push(3);
    structure.push(5);
    structure.push(6);
    structure.push(2);
    structure.push(7);
    structure.push(8);
    setInterval(runLoop,50);
    counter = 1;
}

window.onload = initialize;
