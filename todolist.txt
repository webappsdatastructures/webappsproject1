make the login page

TASK ASSIGNMENTS:
finalize structures
	bst [marco]
	dll [jordan]
	sll [drew]


login page [drew]
test page
	functions running backend [jordan]
	user interface [drew, jordan]
practice page [marco]
database stuff [marco]


DESCRIPTIONS:
once inside: menu of practice or test
	test page (and functions)
	practice page (ui)

testing stuff:
	functionality:
		drag and drop nodes.
		check correct [ add to DataStructure operator== ]

	questions:
		build a structure with this data [array]
		dll and sll: move pointers [order of connections]


databases:
	table users [id, un, pw, deleted]
	table tests [testid, userid, testName, description, passFail, deleted]
	table practicePages [id, userid, pageName, structure, deleted]


STANDARDS:
worry about css later
make sure to push to git when things work!!!
and pull before working on things and before pushing!!!

FUNCTION DECLARATIONS:
	Database stuff:
		bool addUser(string name,string password) done
		bool verifyUser(string name, string password) done

		string getPractices(int id) //echos html table // done
		string getPractice(int practiceId) //returns structure string?
		bool addPractice(string practiceName, int userId, string structure)
		bool deletePractice(int practiceId)

		string getTests(int id)
		string getTest(int id)
		bool addTest(string description, int passFail)
		bool deletePractice(int practiceId)


	Javascript functions:
		string makeStructureString(structure)
		structure makeStructure(string info)
