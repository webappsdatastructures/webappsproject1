$(document).ready(function(){
    $("#mypracticebutton").click(function(){
    	$("#menuoptions").replaceWith(
        	"<h2 style='text-align:center'>My Practice Structures<br></br> </h2>");
        $("h2").append("<div style='text-align:center'><button id='newpractice'>New Practice</button></div>");
        practicemenu();
        printPracticePages();
    });
    $("#testbutton").click(function(){
        $("#menuoptions").replaceWith(
            "<h2 style='text-align:center' font-size:60px>My Tests <br></br></h2>");
        $("h2").append("<div style='text-align:center'><button id='newpractice'>New Test</button></div>");
        testmenu();
        printTestPages();
    });
    $("h2").click(function(){
    	console.log("window");
        window.location.replace("data_practice.php")
    });
    $("#mainmenu").click(function(){
    	window.location.replace("user_menu.php")
    });
});
function practicemenu() {
	$("#newpractice").click(function(){
    	console.log("window");
        window.location.replace("data_practice.php?q=none");
    });
}
function testmenu() {
    $("#newpractice").click(function(){
        console.log("window");
        window.location.replace("data_test.php");
    });
}
function printPracticePages() {
    console.log("printing");
    $.post("ServerSide/PracticePage.php", //marco's print files function
    function(data, status) {
        $("h2").append(data);
    });
    openPracticePages();
}
function printTestPages() {
    console.log();
    $.post("ServerSide/Test.php",
        function (data,status) {
            $("h2").append(data);
        }
        );
}
function openPracticePages() {
    $("tr").click(function(){
        var pageid = $(this).attr('id'); //give each row we're clicking an id
        console.log(pageid);
        $.post("fetchFile.php", //not sure what this file is
        {
            pageid: pageid
        },
        function(data, status) {
            var d = $("#"+pageid).text();
            console.log(d);
            window.location.replace("data_practice.php?q="+d);      
        });
        
    });
}
