<html>
<link rel="stylesheet" type="text/css" href="styles.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="practice_handling.js"></script>


<script type="text/javascript">
function runLoop() {
    context.fillStyle = "white";
    context.fillRect(0,0,canvas.width,canvas.height);
    console.log("filled background "+counter);
    structure.refresh(context,4);
    if (counter % 70 === 0)
    {
        structure.pop();
    }
    counter += 1;
}
function initialize() {
    document.NODE_SIZE = 30;
    canvas = document.getElementById('canv');
    context = canvas.getContext('2d');
    structure = new SinglyLinkedList(400,400,new StackOrdering());
    structure.push(4);
    structure.push(3);
    structure.push(5);
    structure.push(6);
    structure.push(2);
    structure.push(7);
    structure.push(8);
    setInterval(runLoop,50);
    counter = 1;
}
window.onload = initialize;
</script>


<head>
	<title>Data Practice</title>
</head>
<body>
<div id="settings">
	Welcome, user <br>
	Sign Out
</div>
<h1 style="text-align: left; margin-left: 38%;">Data Practice</h1>

<p class="indent"></p>
<div class="control-area">
<label id="select-struct">New Structure</label>
<div class="dropdown">
  <button class="dropbtn" id="structurebutton">Structure</button>
  <div class="dropdown-content">
    <a id="stack" >Stack</a>
    <a id="queue" >Queue</a>
    <a id="bst" >BST</a>
  </div>
</div>
<div class="dropdown">
  <button class="dropbtn" id="listbutton">List</button>
  <div class="dropdown-content">
    <a id="slinked" >Singly Linked</a>
    <a id="dlinked" >Doubly Linked</a>
  </div>
</div>
<div class="dropdown">
  <button class="dropbtn" id="createbutton">Create</button>
</div>
</div>

<div class=center>
	<form id="pushform" autocomplete="off">
		<input type="text" name="push" class="method">
		<button class="method" id="push">Push</button>
	</form>
	  <button class="method" id="pop">Pop</button>
    <button class="method" id="clear">Clear</button>
</div>

<!--<div id=datafield></div>-->
<canvas id="canv" width="800" height="800" style="border:1px solid white;
background-color: white"></canvas>

</body>
</html>