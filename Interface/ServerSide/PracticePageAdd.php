<?php
session_start();
include_once "Connection.php";
include_once "utilities.php";

//set function to run
if(isset($_POST['func']))
{
    if($_POST['func'] == "add_practice_page")
    {
      $userId = $_SESSION['UserId'];

      if(empty($_POST["practicePageName"]))
      $practiceName = "";
      else
      $practiceName = $_POST['practicePageName'];

      if(empty($_POST["structureInfo"]))
      $structureInfo = "";
      else
      $structureInfo = $_POST['structureInfo'];

      if(empty($_POST["oldPracticePageId"]))
      $oldPracticePageId = 0;
      else
      $oldPracticePageId = $_POST['oldPracticePageId'];

      add_practice_page($userId, $practiceName, $structureInfo, $oldPracticePageId);
    }
}

function add_practice_page($userId, $practiceName, $structureInfo, $oldPracticePageId) {
	global $conn;
  $pageAdded = False;
  //sanitize user input
  $practiceName = trim($practiceName);
  $practiceName = mysql_fix_string($practiceName);

  if(verify_page($oldPracticePageId)){
    //if oldPracticePage exists, delete it
    delete_practice_page($oldPracticePageId);
  }



  //add page
  $query = "Insert Into PracticePage Values(0, $userId, '$practiceName', '$structureInfo', 'F')";
  if (!$conn->query($query)) {
      printf("Error: %s\n", $conn->error);
  }
  else{
    $pageAdded = True;
  }

  if($pageAdded){
    echo 1;
  }
  else {
    echo 0;
  }
}

function verify_page($oldPracticePageId) {
  global $conn;

  $pageExists = False;

  $query = "Select * From PracticePage Where Id = $oldPracticePageId";
  $recSet = $conn->query($query);
  $rec = $recSet->fetch_assoc();

  if($rec != null){
    //page Exists
    //echo 1;
    $pageExists = True;
  }
  else {
    //echo 0;
  }
  return $pageExists;
}

function delete_practice_page($practiceId) {
  $pageDeleted = False;

	global $conn;
  $query = "Delete From PracticePage Where Id = $practiceId";
  if (!$conn->query($query)) {
      printf("Error: %s\n", $conn->error);
  }
  else{
    $pageDeleted = True;
  }

  return $pageDeleted;
}
?>
