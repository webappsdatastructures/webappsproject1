
$(document).ready(function(){

    function initializeParams() {
        $("#popup").hide();
        structure = null;
        refStructure = null;
        selected = null;
        md = null;
        showingRef = false;
        let fdsa = makeStructure(strucType, listType);
        structure = fdsa[0];
        refStructure = fdsa[1];
        generateNodes(10);
        structure.push(nodes[0].data);
        refStructure.push(nodes[0].data);
        nodes = nodes.slice(1);
        $("#msgP").html("Insert the nodes below, as they appear. Press submit when finished to grade your response."); 
    }
    $("#mainmenu").click(function(){
        window.location.replace("user_menu.php")
    });
    $("#slinked").click(function(){
        $("#listbutton").html("Singly Linked");
        strucType = "sll";
    });
    $("#pushform").submit(function(event){
        event.preventDefault();
    });
    $("#popform").submit(function(event){
        event.preventDefault();
    });
    $("#dlinked").click(function(){
        strucType = "dll";
        $("#listbutton").html("Doubly Linked");
    });
    $("#stack").click(function(){
        listType = "s";
        $("#structurebutton").html("Stack");
    });
    $("#queue").click(function(){
        listType = "q";
        $("#structurebutton").html("Queue");
    });
    $("#retryBtn").click(initializeParams);
    $("#saveBtn").click(save);
    $("#bst").click(function(){
        strucType = "bst";
        $("#structurebutton").html("BST");
    });
    $("#pq").click(function(){
        listType = "pq";
        $("#structurebutton").html("PQ");
    });
    $("#submitButton").click(function(){
        event.preventDefault();
        if (structure.equals(refStructure)) {
        $("#msgP").html("Correct!");
        }
        else{
        $("#msgP").html("Incorrect Answer. Please view solution or try again.");   
        }
        $("#popup").show();
    });
    $("#solnBtn").click(function(){
        showingRef = true;
    });
    $("#createbutton").click(function(){
        if(structure !== null) {
            //structure.clear();
            structure = null;
        }
        // $("#structurebutton").html("Structure");
        // $("#listbutton").html("List");
        checkBST();
        initialize();
        initializeParams();
        callRunLoop2();

    });
    $("#push").click(function(){
        if(structure !== null) {
            var n = $("#newnode").val();
            $("#newnode").val("");
            if(!isNaN(n)) {
                structure.push(Number(n));    
            }
        } 
    });
    $("#pop").click(function(){
        if(structure !==null) {
            if(strucType=="bst") {
                var n = $("#oldnode").val();
                if(!isNaN(n)) {
                    structure.remove(n);
                }
            } else {
                structure.pop();
            }
        }
    });
    $("#clear").click(function(){
        //structure.clear();
        structure = null;
    });
    $("#savebutton").click(function(e) {
        e.preventDefault();
        var fname = $("#filename").val();
        $("#filename").val("");
        //save file
    });
    let canvas = $('#canv');
    canvas.mousedown(function(e) {
        md = true;
        var x = e.offsetX;// - canvas.val().width/2; //Can pull how to do this from the n-body program
        var y = e.offsetY;// - canvas.val().height/2;
        // console.log(x + ", " + y);
        if (nodes[0].contains(x,y) && nodes.length > 0)
        {
            selected = nodes[0];
        }
    });
    canvas.mouseup(function(e) {
        md = false;
        if (selected !== null)
        {
            var min = (selected.x-structure.children[structure.children.length - 1].x)**2 + (structure.children[structure.children.length - 1].y-selected.y)**2;
            var closest = structure.children[structure.children.length -1];
            var index = structure.children.length - 1;
            for (var i = structure.children.length - 1; i >= 0; i--)
            {
                dist = (selected.x-structure.children[i].x)**2 + (selected.y - structure.children[i].y)**2;
                if (dist < min)
                {
                    closest = structure.children[i];
                    min = dist;
                    index = i;
                }
            }
            structure.push(selected.data,index,selected.x,selected.y,selected.x-structure.children[index].x);
            refStructure.push(selected.data);
            nodes = nodes.slice(1);
            selected = null;
            nodes[0].hardUpdate(50,50);
        }
    });
    canvas.mousemove(function(e) {
        if (selected !== null && md === true)
        {      
            let sz = document.NODE_SIZE/2;
            var x = e.offsetX+sz;// - canvas.val().width/2;
            var y = e.offsetY-sz;// - canvas.val().width/2;
            // console.log(x + ", " + y);
            selected.hardUpdate(x,y);
        }
    });

    function checkBST() {
        if(strucType == "bst") {
            $("#pop").html("Remove");
            $("#oldnode").removeAttr("disabled");
        } else {
            $("#pop").html("Pop");
            $("#oldnode").attr("disabled", "disabled");
        }
    }
    function save() {
        console.log("ServerSide/SaveTest.php?data="+descriptionString()+":" + String(Number((structure.equals(refStructure)))));
        let data = {}
        data['data'] = descriptionString()+":" + String(Number((structure.equals(refStructure))));
        $.post("ServerSide/SaveTest.php",data);
    }
    function descriptionString() {
        var str = "Building a "
        if (strucType == "bst") {
            str += "Binary Search Tree";
            return str;
        }
        if (listType == "q") {str += "Queue-Ordered ";}
        else if (listType == 'pq') {str += "Priority-Queue-Ordered ";}
        else {str += "Stack-Ordered";}
        if (strucType == 'dll') {str += "Doubly-Linked List";}
        else {str += "Singly-Linked List";}
        return str;

    }
    function generateNodes(n = 15) {
        let min = 0;
        n = n+1;
        let max = 2*n;
        var tmpnodes = [];
        nodes = [];
        for (var i = 0; i < n; i++) {
            var tmp = Math.floor(Math.random()*(max - min + 1))+min;
            if (!tmpnodes.includes(tmp))
            {
                tmpnodes.push(tmp);
            }
        }
        if (strucType === 'sll')
        {
            for (var i = 0; i < tmpnodes.length; i++) {
                nodes.push(new SLLNode(-800,-800,tmpnodes[i]));
            }
        }
        else if (strucType === 'dll')
        {
            for (var i = 0; i < tmpnodes.length; i++) {
                nodes.push(new DLLNode(-800,-800,tmpnodes[i]));
            }
        }
        else 
        {
            for (var i = 0; i < tmpnodes.length; i++) {
                nodes.push(new BSTNode(-800,-800,tmpnodes[i]));
            }
        }
        console.log(nodes);
        nodes[1].hardUpdate(50,50);
    }

    $("#popup").hide();

});



