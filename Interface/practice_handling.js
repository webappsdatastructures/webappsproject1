$(document).ready(function(){
    console.log("ready");
    $("#mainmenu").click(function(){
        window.location.replace("user_menu.php")
    });
    $("#slinked").click(function(){
        $("#listbutton").html("Singly Linked");
        strucType = "sll";
    });
    $("#pushform").submit(function(event){
        event.preventDefault();
    });
    $("#popform").submit(function(event){
        event.preventDefault();
    });
    $("#dlinked").click(function(){
        strucType = "dll";
        $("#listbutton").html("Doubly Linked");
    });
    $("#stack").click(function(){
        listType = "s";
        $("#structurebutton").html("Stack");
    });
    $("#queue").click(function(){
        listType = "q";
        $("#structurebutton").html("Queue");
    });
    $("#pq").click(function(){
        listType = "pq";
        $("#structurebutton").html("PQ");
    });
    $("#bst").click(function(){
        strucType = "bst";
        $("#structurebutton").html("BST");
    });
    $("#createbutton").click(function(){
        if(structure !== null) {
            structure = null;
        }
        $("#structurebutton").html("Structure");
        $("#listbutton").html("List");
        checkBST();
        initialize();
        structure = makeStructure(strucType, listType)[0];
        callRunLoop();
    });
    // console.log($("#structdata").html());
    if ($("#structdata").html() !== "" && $("#structdata").html() !== null && $("#structdata").html() !== undefined)
    {
        checkBST();
        initialize();
        let structureInfo = stringToStructure($("#structdata").html());
        console.log(structureInfo);
        fileName = structureInfo['pgName'];
        structId = structureInfo['structId'];
        structure = structureInfo['structure'];
        $(filename).val(fileName);
        callRunLoop();
    }
    $("#push").click(function(){
        if(structure !== null) {
            var n = $("#newnode").val();
            n = n.replace(/\s/g, '');
            $("#newnode").val("");
            if(!isNaN(n) && n!=="") {
                structure.push(Number(n));
            }
        }
    });
    $("#pop").click(function(){
        if(structure !==null) {
            if(strucType=="bst") {
                var n = $("#oldnode").val();
                $("#oldnode").val("");
                console.log(n);
                if(!isNaN(n)) {
                    structure.delete(structure.root, n);
                }
            } else {
                structure.pop();
            }
        }
    });
    $("#clear").click(function(){
        //structure.clear();
        structure = null;
    });
    $("#savebutton").click(function(e) {
        e.preventDefault();
        var fname = $("#filename").val();
        $("#filename").val();
        var strucString = structureToString(structure, structId, fname);

        //call php function to save
        let practicePageName = $("#filename").val();

        let url  = "ServerSide/PracticePageAdd.php";

        let data = {};
        data['func'] = "add_practice_page";
        data['structureInfo'] = strucString;
        data['practicePageName'] = practicePageName;

        //If there is practice page id get else practice page = 0
        data['oldPracticePageId'] = structId;
        // data['userId'] = practicePageName;

        $.post(url, data,
        function(data,status) {
            if(data === "1") {
              alert("Page Saved Successfully!");
            }
            else if(data === "0"){
              alert("Error Saving Page.");
            }
            else{
              alert("error");
            }
        });
    });
    function checkBST() {
        if(strucType == "bst") {
            $("#pop").html("Delete");
            $("#oldnode").removeAttr("disabled");
        } else {
            $("#pop").html("Pop");
            $("#oldnode").attr("disabled", "disabled");
        }
    }
    $("#q").click(function(){
        var str = $("#q").val();
        console.log(str);
        if(str !== "none") {
            checkBST();
            initialize();
            map = stringToStructure(str);
            structure = map.structure;
            fileName = map.pgName;
            structId = map.structId;
            callRunLoop();
            $('#workingfile').html('Working file: '+fileName);
        }
    });
});
