class SinglyLinkedList extends DataStructure {
    constructor(x, y, ordering) {
        super(x, y);
        this.ordering = ordering;
        this.shift = document.NODE_SIZE*2;
    }
    pop() {
        let index = this.ordering.remove();
        if(this.children.length>1) {
            if(index>0) {
                this.children[index-1].updateNext(this.children[index].next.next);
            }
            this.children.splice(index, 1);
            if(this.children.length>0) {
                this.children[this.children.length-1].deletePointer();
            }
            this._movePointers();
            this._updateChildrenPositions();
        } else if(this.children.length==1) {
            this.children.splice(index, 1);
        }
        this._updateChildrenPositions();
    }
    push(elem, hardIndex=-1,x=300,y=150,diff=0) {
        var index = 0;
        if (diff >0)
        {
            index = this.ordering.insert(elem, this.children, hardIndex)+1;
        }
        else
        {
            index = this.ordering.insert(elem, this.children, hardIndex);
        }
        console.log("adding " + index + " of " + this.children.length);
        let nodeX = this.children.length*50 + document.NODE_SIZE;
        let nodeY = this.y;
        let node = new SLLNode(x,y, elem);
        if(index==this.children.length) {
            node.deletePointer();
            if(this.children.length>0) {
                this.children[this.children.length-1].addPointer();
            }
        }
        this.children.splice(index, 0, node);
        this._movePointers();
        this._updateChildrenPositions();
    }
    refresh(context, step) {
        //console.log("refreshing, size: "+this.children.length);
        for(var child=0; child<this.children.length-1; ++child) {
            //console.log("child: "+child);
            this.children[child].refresh(context, step);
        }
        if(this.children.length>0) {
            this.children[this.children.length-1].lastRefresh(context, step);
        }
    }
    insertArray(arr) {
        for (var i = arr.length - 1; i >= 0; i--) {
            this.push(arr[i]);
        }
    }
    _movePointers() {
        console.log("length = " + this.children.length);
        for(var child=0; child < this.children.length-1; child++) {
            this.children[child].updateNext(this.children[child+1]);
        }
    }
    _updateChildrenPositions() {
        for (var child = 0; child < this.children.length; child++) {
            let midpt = this.children.length/2;
            let newX = (this.x+(child-midpt)*this.shift)+(this.shift/(midpt%2+1));
            this.children[child].updatePos(newX, this.y);
        }
    }
    size() {
        return this.children.length;
    }
    printInfo() {
        return "1"+this.ordering.print();
    }
    printChildren() {
        var str = "";
        for (var i = this.children.length - 1; i >= 0; i--) {
                str = str+this.children[i].data+",";
        }
        return str.slice(0,str.length-1);
    }

}
