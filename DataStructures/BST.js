class BST extends DataStructure{
	constructor(x,y){
		console.log("bst created");
    super(x,y);
		this.root = null;
		this.nodeDistX = 40;
		this.nodeDistY = 50;
		this.bstStr = "";
	}
  push(data,index=-1,x=0,y=0,diff=0) {
  	if (index !== -1)
  	{
  		let newNode = new BSTNode(x,y,data);
  		let old = this.children[index];
  		if (diff >=0)
  		{
  			old.right = newNode;
  			old.right.parent = old;
  		}
  		else
  		{
  			old.left = newNode;
  			old.left.parent = old;
  		}
  		this.children.push(newNode);
  		return true;

  	}
  	console.log("pushing");
    if(this.root === null){
      let newNode = new BSTNode(this.x,this.y,data);
      this.root = newNode;
			this.children.push(this.root);
	  this.moveNodes();
      return true;
    }

    let curr = this.root;

    while(curr !== null){
      if(data === curr.data){
        //Can't insert duplicates
        this.moveNodes();
        return false;
      }
      else if(data < curr.data){
        if(curr.left === null){
          curr.left = new BSTNode(curr.x - this.nodeDistX, curr.y+this.nodeDistY, data);
					curr.left.parent = curr;
					this.children.push(curr.left);
			this.moveNodes();
          return true;
        }
        else{
          curr = curr.left;
        }
      }
      else{
        //(data > curr.data)
        if(curr.right === null){
          curr.right = new BSTNode(curr.x + this.nodeDistX, curr.y+this.nodeDistY, data);
					curr.right.parent = curr;
					this.children.push(curr.right);
		  this.moveNodes();
          return true;
        }
        else{
          curr = curr.right;
        }
      }
    }
    this.moveNodes();
    return false;
  }

  moveNodes(){
  	var flag = true;
  	for (var i = this.children.length - 1; i >= 0; i--) {
  		for (var j = this.children.length - 1; j >= 0; j--) {
  			if (i !== j && this.children[i].x === this.children[j].x && this.children[i].y === this.children[j].y && flag) {
  				flag = false;
  				let mid = this.children[i].x
  				for (var x = 0; x <= this.children.length-1; x++) {
  					if (this.children[x].x < mid) {
  						this.children[x].updatePos(this.children[x].x-40,this.children[x].y);
  					}
  				}
  				for (var y = 0; y <= this.children.length-1; y++) {
  					 if (this.children[y].x > mid) {
  						this.children[y].updatePos(this.children[y].x+40,this.children[y].y);
  					}
  				}
  				if (this.children[i].data < this.children[j].data) {
  						this.children[i].updatePos(this.children[i].x-40,this.children[i].y);
  						this.children[j].updatePos(this.children[j].x+40,this.children[j].y);
  				}
  				else	{
  						this.children[i].updatePos(this.children[i].x+40,this.children[i].y);
  						this.children[j].updatePos(this.children[j].x-40,this.children[j].y);  				}
  			}
  		}
  	}
  }

  delete(self, data){
		if(self === null) return false;
		if(data < self.data){
			//traverse left
				this.delete(self.left,data)
		}
		else if(data > self.data){
			//traverse right
				this.delete(self.right,data)
		}
		else{
			//delete value
				if((self.left !== null) && (self.right !== null)){
					let successor = this.findMinNode(self.right);
					self.data = successor.data;
					this.delete(successor,successor.data);
				}
				else if((self.left !== null) && (self.right === null)){
					this.replaceNodeInParent(self, self.left);
				}
				else if((self.left === null) && (self.right !== null)){
					this.replaceNodeInParent(self, self.right);

				}
				else{
					this.replaceNodeInParent(self, null);
				}
		}
  }

	findMinNode(self){
		if(self === null){
			return null;
		}
		else{
			let currNode = self;
			while(currNode.left !== null){
				currNode = currNode.left;
			}
			return currNode;
		}
		return null;
	}

	replaceNodeInParent(self, newVal){
		if(self.parent !== null){
			if(self.data === self.parent.left.data){
				self.parent.left = newVal;
				this.children.splice(this.children.indexOf(self), 1);
			}
			else{
				self.parent.right = newVal;
				this.children.splice(this.children.indexOf(self), 1);
			}
		}
		if(newVal !== null){
			if(self.parent === null) this.root = newVal; //replace root?
			this.children.splice(this.children.indexOf(self), 1);
			newVal.parent = self.parent;
		}
	}

  printInOrder(){
    if(this.root !== null){
      this.printInOrderHelper(this.root);
    }
  }
  printInOrderHelper(currNode){
    if(currNode.left !== null) this.printInOrderHelper(currNode.left);
    console.log(currNode.data);
    if(currNode.right !== null) this.printInOrderHelper(currNode.right);
  }

	getInOrderStr(){
		this.bstStr = "";
		if(this.root !== null){
			this.setBSTStr(this.root);
		}
		return this.bstStr;
	}

	setBSTStr(currNode){
		if(currNode.left !== null) this.setBSTStr(currNode.left);
		this.bstStr += currNode.data + ",";
		if(currNode.right !== null) this.setBSTStr(currNode.right);
	}

	refresh(ctx,step){
		if(this.root !== null){
			this.refreshInOrderHelper(this.root, ctx, step);
		}
	}

	refreshInOrderHelper(currNode, ctx, step){
		if(currNode.left !== null) this.refreshInOrderHelper(currNode.left, ctx, step);
		currNode.refresh(ctx, step);
		if(currNode.right !== null) this.refreshInOrderHelper(currNode.right, ctx, step);
	}

	clear(){
		this.root = null;
		this.children = [];
	}

	getNodes(){
		return this.children;
	}

	equals(other){
		return this.getInOrderStr() === other.getInOrderStr();
	}
    printInfo() {
        return "00";
    }
    printChildren() {
        var str = "";
        for (var i = 0; i < this.children.length; i++) {
                str = str+this.children[i].data+",";
        }
        return str.slice(0,str.length-1);
    }
}
