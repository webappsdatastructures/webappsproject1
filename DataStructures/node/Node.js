class Node {
	constructor(x,y,data) {
		this.x = x;
		this.y = y;
		this.data = data;
		this.futureX = x;
		this.futureY = y;
		this.pointers = [];
	}

	updatePos(x,y) {
		this.futureX = x;
		this.futureY = y;
	}

	hardUpdate(x,y) {
		this.futureX = x;
		this.futureY = y;
		this.x = x;
		this.y = y;
	}

	refresh(context,step) {
		if (this.x!==this.futureX) {
			if (this.x > this.futureX) {
				this.x -= Math.min(step,this.x-this.futureX);
			}
			else {
				this.x += Math.min(step,this.futureX-this.x);
			}
		}
		if (this.y!==this.futureY) {
			if (this.y > this.futureY) {
				this.y -= Math.min(step,this.y-this.futureY);
			}
			else {
				this.y += Math.min(step,this.futureY-this.y);
			}
		}
		for (var i = this.pointers.length - 1; i >= 0; i--) {
			this.pointers[i].refresh(context,step);
		}
		this.draw(context);
	}

	draw(context) {
		context.fillStyle = "black";
		let sz = document.NODE_SIZE/2;
		context.rect(this.x-sz,this.y+sz,document.NODE_SIZE,document.NODE_SIZE);
        context.stroke();
		context.fillText(this.data,this.x-sz/2,this.y+30);
		for (var i = this.pointers.length - 1; i >= 0; i--) {
			this.pointers[i].draw(context);
		}
        // console.log("filled node");
	}

	contains(x,y) {
		let sz = document.NODE_SIZE/2;
		console.log(this.x + "," + this.y + "in " + (this.x+document.NODE_SIZE) + "," + (this.y+document.NODE_SIZE))
		return x+sz >= this.x && x+sz <= this.x + document.NODE_SIZE && y-sz >= this.y && y-sz <= this.y + document.NODE_SIZE;
	}
}
