class BSTNode extends Node {
  constructor(x,y,data){
    super(x,y,data);
    this.left = null;
    this.right = null;
    this.parent = null;
    this.pointers[0] = new Pointer(this.getLeftAnchor()[0],this.getLeftAnchor()[1],this.getLeftAnchor()[0],this.getLeftAnchor()[1]);
    this.pointers[1] = new Pointer(this.getRightAnchor()[0],this.getRightAnchor()[1],this.getRightAnchor()[0],this.getRightAnchor()[1]);
  }

  updateNext(node) {
    this.next = node;
  }

  updatePrev(node) {
    this.prev = node;
  }

  getRightAnchor() {
    return [this.x+document.NODE_SIZE/2,this.y+document.NODE_SIZE]; //Might not be legit JS?????
  }

  getLeftAnchor() {
    return [this.x-document.NODE_SIZE/2,this.y+document.NODE_SIZE]; //Might not be legit JS?????
  }

  getTopAnchor() {
    return [this.x,this.y+document.NODE_SIZE/2]; //Might not be legit JS?????
  }

  refresh(context,step) {
    if(this.left !== null) {
      this.pointers[0].updateOrigin(this.getLeftAnchor()[0],this.getLeftAnchor()[1]+5);
      this.pointers[0].updateDest(this.left.getTopAnchor()[0],this.left.getTopAnchor()[1]);
    }
    else {
      this.pointers[0].updateOrigin(this.getLeftAnchor()[0],this.getLeftAnchor()[1]+5);
      this.pointers[0].updateDest(this.getLeftAnchor()[0],this.getLeftAnchor()[1]+5);
    }
    if(this.right !== null) {
      this.pointers[1].updateOrigin(this.getRightAnchor()[0],this.getRightAnchor()[1]-5);
      this.pointers[1].updateDest(this.right.getTopAnchor()[0],this.right.getTopAnchor()[1]);
    }
    else {
      this.pointers[1].updateOrigin(this.getRightAnchor()[0],this.getRightAnchor()[1]-5);
      this.pointers[1].updateDest(this.getRightAnchor()[0],this.getRightAnchor()[1]-5);
    }
    super.refresh(context,step);
    this.draw(context);
  }
}
